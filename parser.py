#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sunday Jan 27

@author: andrei
"""

import feedparser
import pygsheets


def createSheets():
    c = pygsheets.authorize()

    # Открываем книгу по названию
    sh = c.open('rss')

    wks_rss = sh.worksheet_by_title("feeds")

    all_feeds = [i[0] for i in wks_rss.get_all_values(
        include_tailing_empty=False, include_tailing_empty_rows=False)]
    wks_list = sh.worksheets()

    [sh.add_worksheet(feed) for feed in all_feeds if feed not in [
        wks.title for wks in wks_list]]

    return all_feeds


def updateSheet(sheet, feed_items):
    c = pygsheets.authorize()

    # Открываем книгу по названию
    sh = c.open('rss')

    wks_data = sh.worksheet_by_title(sheet)

    values = wks_data.get_all_values(
        include_tailing_empty=False, include_tailing_empty_rows=False)

    print(sheet)

    if len(values[0]) > 0:
        feed_ids = [i[0] for i in values]
    else:
        feed_ids = []

    rows_before = len(feed_ids) + 1
    updates = []

    for item in feed_items:
        if item[0] not in feed_ids:
            updates.append(item)

    rows_num = rows_before + len(updates)

    # Добавляем данные в таблицу Google sheets

    if len(updates) > 0:
        wks_data.update_values('A%s:C%s' % (rows_before, rows_num), updates)


def main():
    for rss in createSheets():

        # Парсим ленту RSS
        d = feedparser.parse(rss)
        values = []

        # Выбираем поля из ленты
        [values.append([ent.id, ent.title, ent.link]) for ent in d.entries]

        # Вставляем новые записи в таблицу Google sheets
        updateSheet(rss, values)

if __name__ == "__main__":
    main()
